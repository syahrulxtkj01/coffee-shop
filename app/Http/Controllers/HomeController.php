<?php

namespace App\Http\Controllers;

use App\Category;
use App\Price;
use App\Product;
use App\Topping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $data['max'] = Product::max('id');

        // $produk = Product::all()->count();

        // for ($x = 1; $x <= $produk; $x++) {

        //     $product[$x] = Product::find($x);
        // }

        $produk = Product::max('id');

        for ($x = 1; $x <= $produk; $x++) {

            $products[$x] = Product::find($x);

            if ($products[$x] == null) {
                continue;
            } else {
                $product[$x] = Product::find($x);
            }
        }

        $data['category'] = Category::all();

        $data['toppings'] = Topping::all();

        



        // return $product[2];


        // $data['produks'] = Price::join('products', 'products.id', 'prices.product_id')
        //     ->select('prices.*', 'products.*')->get();
        return view('pages.home', ['product' => $product,'products' => $products], $data);


        // $data['products'] = DB::table('prices')
        //     ->join('products', 'products.id', '=', 'prices.product_id')
        //     ->select('products.*', 'prices.*')
        //     ->groupBy('products.id')
        //     ->get();


        // $data['products'] = Price::groupBy('product_id')->select('product_id')->get();

        // $product = Price::join('products', 'products.id', 'prices.product_id')
        //     ->select('products.*', 'prices.*')->first();
        // $this->product['products'] = $product;

        // return $this->product;
        // return view('pages.home', $this->product);
        // return view('pages.home', $data);
    }

    public function test()
    {
        $produk = Product::max('id');

        for ($x = 1; $x <= $produk; $x++) {

            $products[$x] = Product::find($x);

            if ($products[$x] == null){
                continue;
            }

            else{
                $product[$x] = Product::find($x);
            }

            
        }


        return $product;
    }
}
