<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Topping;
use Illuminate\Http\Request;

class ToppingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['toppings'] = Topping::all();
        return view('pages.admin.topping.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.admin.topping.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'topping_name', 'price' => 'required',
        ]);

        $topping = new Topping();
        $topping->name = $request->topping_name;
        $topping->price = $request->price;

        // return $topping;

        $topping->save();
        alert()->success('Sukses !', 'Topping Ditambahkan.');
        return redirect()->route('topping.create');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Topping::findOrFail($id);
        return view(
            'pages.admin.topping.edit',
            [
                'item' => $item
            ],

        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Topping::where('id', $id)
            ->update([
                'name' => $request->topping_name,
                'price' => $request->price
            ]);

        alert()->success('Sukses !', 'Topping Berhasil diubah.');
        return redirect()->route('topping.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $topping = Topping::findOrFail($request->id);
        $topping->delete();
        alert()->success('Sukses !', 'Produk Berhasil dihapus.');
        return redirect()->route('topping.index');
    }
}
