<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Price;
use App\Product;
use Illuminate\Http\Request;

use function GuzzleHttp\Promise\all;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data['prices'] = Price::all();
        

        $price = Price::all();
        // $harga = Price::all()->count();
        // // return $harga;

        // for ($x = 1; $x <= $harga; $x++) {

        //     $price[$x] = Price::find($x);
        // }
        return view('pages.admin.price.index', ['price' => $price]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['category'] = Category::all();

        $data['product'] = Category::join('products', 'products.category_id', 'categories.id')
            ->select('products.*')->get();



        return view('pages.admin.price.create', $data);

        // $data['products'] = Product::all();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'drink', 'drink_price' => 'required',
        ]);

        $price = new Price();
        $price->product_id = $request->drink;
        $price->type = $request->type;
        $price->size = $request->size;
        $price->price = $request->drink_price;

        // return $price;


        $price->save();
        alert()->success('Sukses !', 'Harga Ditambahkan.');
        return redirect()->route('price.create');

        // $data['product'] = Category::join('products', 'products.category_id', 'categories.id')
        //     ->select('products.*')->get();

        // return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Category::join('products', 'products.category_id', 'categories.id')
            ->select('products.*')->where('products.category_id', $id)->get();

        echo json_encode($data);



        // echo $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $item = Price::findOrFail($id);
        return view(
            'pages.admin.price.edit',
            [
                'item' => $item
            ],

        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Price::where('id', $id)
            ->update([
                'type' => $request->type,
                'size' => $request->size,
                'price' => $request->drink_price
            ]);

        alert()->success('Sukses !', 'Minuman Berhasil diubah.');
        return redirect()->route('price.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $product = Product::find($request->id);
        // $product->price()->where('product_id',17);

        $price = Price::find($id);

        // // $price->product()->where('product_id', 17);
        $price->delete();
        alert()->success('Sukses !', 'Harga Berhasil dihapus.');
        return redirect()->route('price.index');
        // return $id;

        // dd($request->id);
    }







    public function cariminuman()
    {
        $data = Category::join('products', 'products.category_id', 'categories.id')
            ->select('products.*')->get();

        // return $data;
        echo "hello ";
        // return response()->json([
        //     'success' => true,
        //     'message' => 'List Data admin',
        //     'data'    => $data
        // ], 200);
    }
}
