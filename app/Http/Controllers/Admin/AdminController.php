<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Topping;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Menjumlah seluruh data yang ada di table Produk
        $data['drinks'] = Product::all()->count();

        //Menjumlah seluruh data yang ada di table Category
        $data['categories'] = Category::all()->count();

        //Menjumlah seluruh data yang ada di table Topping
        $data['topings'] = Topping::all()->count();

        //Menampilkan Halaman Dashboard yang berada pada folder pages/admin dan mengirimkan variable $data
        return view('pages.admin.dashboard', $data);
    }



}
