<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Price;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        // $data['product'] = Price::join('products', 'products.id', 'prices.product_id')
        //     ->select('products.*', 'prices.*')->get();

        return view('pages.admin.product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['category'] = Category::all();

        return view('pages.admin.product.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'drink_name', 'category' => 'required',
        ]);

        $product = new Product();
        $product->name = $request->drink_name;
        $product->flavour = $request->flavour;
        $product->category_id = $request->category;

        // return $product;

        $product->save();
        alert()->success('Sukses !', 'Minuman Ditambahkan.');
        return redirect()->route('product.create');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['category'] = Category::all();

        $item = Product::findOrFail($id);
        return view(
            'pages.admin.product.edit',
            [
                'item' => $item
            ],
            $data

        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Product::where('id', $id)
            ->update([
                'name' => $request->drink_name,
                'flavour' => $request->flavour,
                'category_id' => $request->category
            ]);

        alert()->success('Sukses !', 'Minuman Berhasil diubah.');
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        
        $price = Price::join('products','products.id','prices.product_id')
        ->where('prices.product_id',$id)->delete();
        
        $product->delete();
        alert()->success('Sukses !', 'Minuman Berhasil dihapus.');
        return redirect()->route('product.index');
        // return $product;
    }
}
