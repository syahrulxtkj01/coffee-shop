<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    public function price()
    {
        return $this->HasMany(Price::class);
    }
}
