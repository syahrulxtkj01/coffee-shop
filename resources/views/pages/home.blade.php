@extends('layouts.app')

@section('title')
WAGHE COFFEE & EATERY
@endsection

@section('content')

<header id="header" id="home">
	<div class="header-top">
		<div class="container">
			<div class="row justify-content-end">
				<div class="col-lg-8 col-sm-4 col-8 header-top-right no-padding">
					<ul>
						<li>
							Everyday : 10am to 10pm
						</li>
						<li>
							<a href="tel:(012) 6985 236 7512">(012) 6985 236 7512</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row align-items-center justify-content-between d-flex">
			<div id="logo">
				<a href="index.html"><img src="{{asset('user_css/img/logo.png')}}" alt="" title="" /></a>
			</div>
			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<li class="menu-active"><a href="#home">Home</a></li>
					<li><a href="#about">About</a></li>
					<li><a href="#coffee">Coffee</a></li>
					<li><a href="#gallery">Gallery</a></li>
					<li><a href="#branch">Our Branch</a></li>
				</ul>
			</nav><!-- #nav-menu-container -->
		</div>
	</div>
</header><!-- #header -->


<!-- start banner Area -->
<section class="banner-area" id="home">
	<div class="container">
		<div class="row fullscreen d-flex align-items-center justify-content-start">
			<div class="banner-content col-lg-7">
				<h6 class="text-white text-uppercase">Now you can feel the Energy</h6>
				<h1>
					Start your day with <br>
					a black Coffee
				</h1>
				<a href="https://gofood.link/a/yM9vxx3" class="primary-btn text-uppercase" target="_blank">Order Now by GoFood</a>
			</div>
		</div>
	</div>
</section>
<!-- End banner Area -->

<!-- Start video-sec Area -->
<section class="video-sec-area pb-100 pt-40" id="about">
	<div class="container">
		<div class="row justify-content-start align-items-center">
			<div class="col-lg-6 video-right justify-content-center align-items-center d-flex">
				<div class="overlay overlay-bg"></div>
				<a class="play-btn" href="{{asset('video/intro.mp4')}}"><img class="img-fluid" src="{{asset('user_css/img/play-icon.png')}}" alt=""></a>
			</div>
			<div class="col-lg-6 video-left">
				<h6>Live Coffee making process.</h6>
				<h1>We Telecast our <br>
					Coffee Making Live</h1>
				<p><span>We are here to listen from you deliver exellence</span></p>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.
				</p>
				<img class="img-fluid" src="{{asset('user_css/img/signature.png')}}" alt="">
			</div>
		</div>
	</div>
</section>
<!-- End video-sec Area -->

<!-- Start menu Area -->
<section class="menu-area section-gap" id="coffee">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="menu-content pb-60 col-lg-10">
				<div class="title text-center">
					<h1 class="mb-10">What kind of Coffee we serve for you</h1>
					<p>Who are in extremely love with eco friendly system.</p>
				</div>
			</div>
			<div class="col-md-12 nav-link-wrap mb-5 ">
				<div class="nav ftco-animate nav-pills justify-content-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">

						<!-- <a class="nav-link active " id="pills-kopi-tab" data-toggle="pill" href="#pills-kopi" role="tab" aria-controls="pills-kopi" aria-selected="true">Kopi</a> -->

						@foreach ($category as $categories )



						<li class="nav-item">
							<a class="nav-link " id="pills-{{$categories->slug}}-tab" data-toggle="pill" href="#pills-{{$categories->slug}}" role="tab" aria-controls="pills-{{$categories->slug}}" aria-selected="true">{{$categories->name}}</a>
						</li>


						@endforeach

					</ul>
				</div>


			</div>
		</div>

		<div class="d-flex justify-content-center">
			<div class="tab-pane fade show active" id="v-pils-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
				<div class="tab-content" id="pills-tabContent">

					@foreach ( $category as $categories )

					<div class="tab-pane fade" id="pills-{{$categories->slug}}" role="tabpanel" aria-labelledby="pills-{{$categories->slug}}-tab">
						<div class="row">
							@for ($z = 1; $z <= $max ; $z++) 
							@if ($products[$z]===NULL) 
							
							@else 
							@if ($product[$z]->category_id == $categories->id)
								<div class="col-lg-4">
									<div class="single-menu" style="height: 300px; ">
										<div class="title-div justify-content-between d-flex">
											<h4>
												{{$product[$z] -> name}}
											</h4>
										</div>
										<p style="margin: -20px 0 20px 0">
											{{$product[$z]->flavour}}
											<!-- Usage of the Internet is becoming more common due to rapid advance. -->
										</p>
										

										@foreach ( $product[$z]->price as $item )

										<div class="title-div justify-content-between d-flex">
											<h4>
												@if ($item->type == '')

												@elseif ($item->type == '1')
												Hot

												@else
												Ice

												@endif
											</h4>
											<p class="float-center">
												@if ($item->size == '')

												@elseif ($item->size == '1')
												Reguler

												@else
												Large

												@endif
											</p>
											<p class="price float-right">
												{{$item->price}}
											</p>
										</div>
										@endforeach
										<p style="margin: 0 0 20px 0" >
											<!-- Usage of the Internet is becoming more common due to rapid advance. -->
										</p>

									</div>
								</div>
							@endif
							@endif




							@endfor
						</div>


					</div>

					@endforeach


				</div>


			</div>
		</div>


		<div class="row d-flex justify-content-center">
			<div class="menu-content pb-60 col-lg-10">
				<div class="title text-center">
					<h1 class="mb-10">Topping</h1>
					<p>Kami juga memberikan berbagai pilihan topping agar minuman anda semakin nikmat</p>
				</div>
			</div>
			<div class="col-md-12 nav-link-wrap mb-5 ">
				<div class="row">
					<div class="col-lg-4 offset-lg-4">
						<div class="single-menu">

							@foreach ( $toppings as $item )
							<div class="title-div justify-content-between d-flex">


								<h4>{{$item->name}}</h4>
								<p class="price float-right">
									{{$item->price}}
								</p>


							</div>
							@endforeach

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	</div>
</section>
<!-- End menu Area -->

<!-- Start gallery Area -->
<section class="gallery-area section-gap" id="gallery">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="menu-content pb-60 col-lg-10">
				<div class="title text-center">
					<h1 class="mb-10">What kind of Coffee we serve for you</h1>
					<p>Who are in extremely love with eco friendly system.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<a href="{{asset('user_css/img/1.jpg')}}" class="img-pop-home">
					<img class="img-fluid" src="{{asset('user_css/img/1.jpg')}}" alt="">
				</a>
				<a href="{{asset('user_css/img/5.jpg')}}" class="img-pop-home">
					<img class="img-fluid" src="{{asset('user_css/img/5.jpg')}}" alt="">
				</a>
			</div>
			<div class="col-lg-8">
				<a href="{{asset('user_css/img/3_1.jpg')}}" class="img-pop-home">
					<img class="img-fluid" src="{{asset('user_css/img/3_1.jpg')}}" alt="">
				</a>
				<div class="row">
					<div class="col-lg-6">
						<a href="{{asset('user_css/img/2.jpg')}}" class="img-pop-home">
							<img class="img-fluid" src="{{asset('user_css/img/2.jpg')}}" alt="">
						</a>
					</div>
					<div class="col-lg-6">
						<a href="{{asset('user_css/img/6.jpg')}}" class="img-pop-home">
							<img class="img-fluid" src="{{asset('user_css/img/6.jpg')}}" alt="">
						</a>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
<!-- End gallery Area -->


<!-- Start branch Area -->
<section class="branch-area section-gap" id="branch">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="menu-content pb-60 col-lg-10">
				<div class="title text-center">
					<h1 class="mb-10">Cabang Kami</h1>
					<p>Saat ini Kami sudah mempunyai 2 cabang yang berada di daerah Bekasi Utara</p>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: -50px;">
			<div class="col-md-12 ftco-animate">
				<div class="row no-gutters justify-content-center">

					<iframe src="https://www.google.co.id/maps/d/u/0/embed?mid=18wpESYacGTbCpXhjlEu3XT5SOn2gbNy9" width="640" height="480"></iframe>

				</div>
			</div>

		</div>


	</div>
</section>
<!-- End branch Area -->


<!-- start footer Area -->
<footer class="footer-area section-gap">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-6 col-sm-6">
				<div class="single-footer-widget">
					<h6>About Us</h6>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.
					</p>
					<p class="footer-text">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<script>
							document.write(new Date().getFullYear());
						</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					</p>
				</div>
			</div>

			<div class="col-lg-2 col-md-6 offset-md-4 col-sm-6 social-widget">
				<div class="single-footer-widget">
					<h6>Follow Us</h6>
					<p>Let us be social</p>
					<div class="footer-social d-flex align-items-center">
						<a href="https://www.instagram.com/waghecoffee/" target="_blank"><i class="fa fa-instagram"></i></a>
						<a href="mailto:waghecoffee@gmail.com"><i class="fa fa-envelope"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- End footer Area -->

@endsection