@extends('layouts.admin')

@section('active')

<li class="nav-item active">
    <a class="nav-link" href="{{ url('/admin') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<li class="nav-item ">
    <a class="nav-link" href="{{ route('product.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Minuman</span></a>
</li>

<li class="nav-item ">
    <a class="nav-link" href="{{ route('topping.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Topping</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('category.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Kategori</span>
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('price.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Harga</span>
    </a>
</li>

@endsection

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    </div>

    <!-- Content Row -->
    <div class="row">

        <!-- Total Minuman -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 ">Total Minuman</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800 ">{{$drinks}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Total Kategori -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 ">Total Kategori</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800 ">{{$categories}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Total Toping -->
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 ">Total Toping</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800 ">{{$topings}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.container-fluid -->
@endsection