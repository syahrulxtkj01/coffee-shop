@extends('layouts.admin')

@section('active')

<li class="nav-item ">
    <a class="nav-link" href="{{ url('/admin') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<li class="nav-item ">
    <a class="nav-link" href="{{ route('product.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Minuman</span></a>
</li>

<li class="nav-item ">
    <a class="nav-link" href="{{ route('topping.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Topping</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('category.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Kategori</span>
    </a>
</li>

<li class="nav-item active">
    <a class="nav-link" href="{{ route('price.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Harga</span>
    </a>
</li>

@endsection

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Harga Minuman</h1>
        <nav aria-label="breadcrumb text-right">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('category.index') }}">Harga</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tambah</li>
            </ol>
        </nav>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Form Tambah Harga Minuman</h6>
                </div>
                <div class="card-body">
                    <form enctype="multipart/form-data" class="form-horizontal" action="{{route('price.store')}}" method="POST">
                        @csrf
                        <div class="row">


                            <div class="form-group col-md-6">
                                <label for="category">Kategori</label>
                                <select class="form-control" id="category" name="category" onchange="changeFunc();">
                                    <option value="">-- Pilih Kategori --</option>
                                    @foreach ($category as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="category">Minuman</label>
                                <div id="drinks">
                                    <select class="form-control" id="drink" name="drink" disabled>
                                        <option value="">-- Pilih Minuman --</option>
                                    </select>
                                </div>
                                <!-- <select class="form-control" id="drink" name="drink">
                                    <option value="">-- Pilih Minuman --</option>
                                    @foreach ($product as $item)
                                    @php

                                    @endphp
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select> -->
                            </div>

                            <div class="form-group col-md-6">
                                <label for="category">Type (Optional)</label>
                                <select class="form-control" id="type" name="type">
                                    <option value="">-- Pilih Tipe --</option>
                                    <option value="1">Hot</option>
                                    <option value="2">Ice</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="category">Size (Optional) </label>
                                <select class="form-control" id="size" name="size">
                                    <option value="">-- Pilih Size --</option>
                                    <option value="1">Reguler</option>
                                    <option value="2">Large</option>
                                </select>
                            </div>


                            <div class="form-group col-md-12">
                                <label for="drink">Harga Minuman</label>
                                <input type="number" class="form-control" id="drink_price" name="drink_price" placeholder="ex : 10000" required>
                                <p id="test1"></p>
                            </div>

                            <button type="submit" class="btn btn-primary col-lg-3 offset-lg-4">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@endsection

@section('addScript')
<script type="text/javascript">
    function changeFunc() {
        string = '';
        var s = document.getElementById('category').value;

        var ajax = new XMLHttpRequest();
        var method = "GET";
        var url = '/admin/price/' + s;
        var asynchronous = true;

        ajax.open(method, url, asynchronous);

        ajax.send();

        if (s === '') {
            string = "<select class = 'form-control' id = 'drink' name = 'drink' disabled > <option value = ''> --Pilih Minuman-- </option> </select> "
            document.getElementById('drinks').innerHTML = string;
        }




        ajax.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

                var data = JSON.parse(this.responseText);
                console.log(data);



                string = "";


                for (z = 0; z < data.length; z++) {
                    // x += data[z].name;
                    string = string + "<option value='" + data[z].id + "'>" + data[z].name + "</option>";
                }

                // var id = [data.id];
                // var name = [date.name];
                // console.log(name);
                // for (i = 0; i < x.length; i++) {

                //     string = string + "<option value='" + id + "'>" + name[i] + "</option>";
                // }
                string = "<select class='form-control' id='drink' name='drink'>" + string + "</select>";

                document.getElementById('drinks').innerHTML = string;



            }



        }









    }
</script>
@endsection