@extends('layouts.admin')

@section('active')

<li class="nav-item ">
    <a class="nav-link" href="{{ url('/admin') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<li class="nav-item active">
    <a class="nav-link" href="{{ route('product.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Minuman</span></a>
</li>

<li class="nav-item ">
    <a class="nav-link" href="{{ route('topping.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Topping</span></a>
</li>

<li class="nav-item ">
    <a class="nav-link" href="{{ route('category.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Kategori</span>
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('price.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Harga</span>
    </a>
</li>

@endsection

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Ubah Minuman</h1>
        <nav aria-label="breadcrumb text-right">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{ route('product.index') }}">Minuman</a></li>
                <li class="breadcrumb-item active" aria-current="page">Ubah</li>
            </ol>
        </nav>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Form Ubah Minuman</h6>
                </div>
                <div class="card-body">
                    <form enctype="multipart/form-data" class="form-horizontal" action="{{route('product.update',$item->id)}}" method="POST">
                        @method('put')
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="drink">Nama Minuman</label>
                                <input type="text" class="form-control" id="drink_name" name="drink_name" value="{{$item->name}}" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="flavour">Varian Rasa (Optional)</label>
                                <input type="text" class="form-control" id="flavour" name="flavour" value="{{$item->flavour}}">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="category">Kategori</label>
                                <select class="form-control" id="category" name="category">
                                    <option value="">-- Pilih Kategori --</option>
                                    @foreach ($category as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary col-12">Ubah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@endsection