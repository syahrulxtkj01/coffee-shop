@extends('layouts.admin')

@section('active')

<li class="nav-item ">
    <a class="nav-link" href="{{ url('/admin') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

<li class="nav-item active">
    <a class="nav-link" href="{{ route('product.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Minuman</span></a>
</li>

<li class="nav-item ">
    <a class="nav-link" href="{{ route('topping.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Topping</span></a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('category.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Kategori</span>
    </a>
</li>

<li class="nav-item">
    <a class="nav-link" href="{{ route('price.index') }}">
        <i class="fas fa-fw fa-utensils"></i>
        <span>Harga</span>
    </a>
</li>

@endsection


@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Daftar Minuman</h1>
        <nav aria-label="breadcrumb text-right">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                <li class="breadcrumb-item active" aria-current="page">Minuman</li>
            </ol>
        </nav>
    </div>

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <a href="{{ route('product.create') }}" class="btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-plus fa-sm text-white-50"></i> Tambah Minuman
        </a>
    </div>

    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="example" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Rasa</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($products as $item)
                        <tr>
                            <td>{{$item->name}}</td>
                            <td>{{$item->flavour}}</td>

                            <td class="text-center">
                                <div class="btn-group">
                                    <div class="col-md-6 mt-2">

                                        <a href="{{route('product.edit',$item->id)}}" class="btn btn-primary fas fa-edit pr-1" style="color: aliceblue">Ubah</a>

                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <button class="btn btn-danger" data-toggle="modal" data-id="{{ $item->id }}" data-target="#deleteModal">
                                            <i class="fa fa-trash pr-1">Hapus</i>
                                        </button>
                                    </div>

                                    <!-- Delete Modal-->
                                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Produk</h5>
                                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">Apakah anda yakin ingin menghapusnya?</div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                                                    <form action="{{ route('product.destroy',$item->id) }}" method="POST" class="delete">
                                                        @csrf
                                                        @method('delete')
                                                        <input type="hidden" name="id" id="input-id">
                                                        <button class="btn btn-primary btn-delete">Hapus</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
@endsection