<div class="tab-pane show active" id="" role="tabpanel" aria-labelledby="pills-kopi-tab">
    <div class="row">
        @for ($y = 1; $y <= 9; $y++) <div class="col-lg-4">
            @php
            $rand = rand ( 1 , $count )
            @endphp
            <div class="single-menu" style="height: 300px;">
                <div class="title-div justify-content-between d-flex">
                    <h4>
                        {{$product[$rand] -> name}}
                    </h4>
                </div>

                @foreach ( $product[$rand]->price as $item )

                <div class="title-div justify-content-between d-flex">
                    <h4>
                        @if ($item->type == '')

                        @elseif ($item->type == '1')
                        Hot

                        @else
                        Ice

                        @endif
                    </h4>
                    <p class="float-center">
                        @if ($item->size == '')

                        @elseif ($item->size == '1')
                        Reguler

                        @else
                        Large

                        @endif
                    </p>
                    <p class="price float-right">
                        {{$item->price}}
                    </p>
                </div>
                @endforeach
                <p>
                    {{$item->flavour}}
                </p>
            </div>
    </div>

    @endfor
</div>

<!-- 

    <div class="tab-pane fade " id="pills-kopi" role="tabpanel" aria-labelledby="pills-kopi-tab">
					<div class="row">

						@for ($z = 1; $z <= $max ; $z++) 

						@if ($products[$z] === NULL  )
							
						@else
						@if ($product[$z]->category_id == 1)
							<div class="col-lg-4">
								<div class="single-menu" style="height: 300px;">
									<div class="title-div justify-content-between d-flex">
										<h4>
											{{$product[$z] -> name}}
										</h4>
									</div>

									<p style="margin: -20px 0 20px 0">
										{{$product[$z] -> flavour}}
									</p>

									@foreach ( $product[$z]->price as $item )

									<div class="title-div justify-content-between d-flex">
										<h4>
											@if ($item->type == '')

											@elseif ($item->type == '1')
											Hot

											@else
											Ice

											@endif
										</h4>
										<p class="float-center">
											@if ($item->size == '')

											@elseif ($item->size == '1')
											Reguler

											@else
											Large

											@endif
										</p>
										<p class="price float-right">
											{{$item->price}}
										</p>
									</div>
									@endforeach

								</div>
							</div>
						@endif
						@endif
						
						


						@endfor



					</div>

				</div>
-->