<script src="{{asset('user_css/js/vendor/jquery-2.2.4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{asset('user_css/js/vendor/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="{{asset('user_css/js/easing.min.js')}}"></script>
<script src="{{asset('user_css/js/hoverIntent.js')}}"></script>
<script src="{{asset('user_css/js/superfish.min.js')}}"></script>
<script src="{{asset('user_css/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('user_css/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('user_css/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('user_css/js/jquery.sticky.js')}}"></script>
<script src="{{asset('user_css/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('user_css/js/parallax.min.js')}}"></script>
<script src="{{asset('user_css/js/waypoints.min.js')}}"></script>
<script src="{{asset('user_css/js/jquery.counterup.min.js')}}"></script>
<script src="{{asset('user_css/js/mail-script.js')}}"></script>
<script src="{{asset('user_css/js/main.js')}}"></script>