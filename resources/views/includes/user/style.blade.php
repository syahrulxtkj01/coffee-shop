<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
<!--
			CSS
			============================================= -->
<link rel="stylesheet" href="{{asset('user_css/css/linearicons.css')}}">
<link rel="stylesheet" href="{{asset('user_css/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('user_css/css/bootstrap.css')}}">
<link rel="stylesheet" href="{{asset('user_css/css/magnific-popup.css')}}">
<link rel="stylesheet" href="{{asset('user_css/css/nice-select.css')}}">
<link rel="stylesheet" href="{{asset('user_css/css/animate.min.css')}}">
<link rel="stylesheet" href="{{asset('user_css/css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('user_css/css/main.css')}}">