<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//User

Route::get('/', 'HomeController@index')->name('home');

Route::get('/test','HomeController@test');


//Admin
Route::prefix('admin')
    ->namespace('Admin')->middleware(['auth'])
    ->group(function () {
        Route::get('/', 'AdminController@index')->name('dashboard');

        Route::resource("product", "ProductController");

        Route::resource("topping", "ToppingController");

        Route::resource("category", "CategoryController");

        Route::resource("price", "PriceController");
    });

Auth::routes(['register' => false, 'reset' => false]);


// Route::get('/home', 'HomeController@index')->name('home');
